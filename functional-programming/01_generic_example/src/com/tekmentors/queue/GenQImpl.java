package com.tekmentors.queue;

public class GenQImpl<T> implements IGenQ<T> {
    private T q[];  //this array holds the queue
    private int putloc, getloc;  //put and get indices

    public GenQImpl(T[] q) {
        this.q = q;
        this.getloc = 0;
        this.putloc = 0;
    }

    @Override
    public void put(T ch) throws QueueFullException {
        if(putloc == q.length){
            throw new QueueFullException(q.length);
        }
        else
            q[putloc++] = ch;
    }

    @Override
    public T get() throws QueueEmptyException {
        if(getloc == putloc)
            throw new QueueEmptyException("Queue is empty");
        return q[getloc++];
    }
}

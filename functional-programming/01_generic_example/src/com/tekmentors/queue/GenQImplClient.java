package com.tekmentors.queue;

public class GenQImplClient {

    public static void main(String[] args){
        Integer[] numbers = new Integer[5];

        GenQImpl<Integer> queue = new GenQImpl<>(numbers);

        for (int i=0; i< 7;i++){
            try {
                queue.put(i);
            } catch (QueueFullException e) {
                System.out.println("i : "+i);
                System.out.println("Exception: "+e);

            }
        }
        for (int i=0; i< 7;i++){
            try {
                queue.get();
            } catch (QueueEmptyException e) {
                System.out.println("i : "+i);
                System.out.println("Exception: "+e.getMessage());

            }
        }
    }
}

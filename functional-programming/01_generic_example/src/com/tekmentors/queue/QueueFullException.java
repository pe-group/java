package com.tekmentors.queue;

public class QueueFullException extends Exception {
    int size;

    public QueueFullException( int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Queue is full. Maximum size is "+size;
    }
}

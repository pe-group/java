package com.tekmentors;

public class Gen<T> { // Declare a generic class. T is the generic type parameter
    T ob;    // declare an object of type T

    public Gen(T ob) {
        this.ob = ob;
    }

    public T getOb() {
        return ob;
    }

    void showType(){
        System.out.println("Type of T is "+ob.getClass().getName());
    }
}

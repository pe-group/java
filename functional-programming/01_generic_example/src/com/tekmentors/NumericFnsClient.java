package com.tekmentors;

public class NumericFnsClient {

    public static void main(String[] args){
        NumericFns<Integer> iOb = new NumericFns<>(25);
        System.out.println("reciprocal : "+iOb.reciprocal());
        System.out.println("fraction : "+iOb.fraction());

        NumericFns<Double> dOb = new NumericFns<>(25.5);
        System.out.println("reciprocal : "+dOb.reciprocal());
        System.out.println("fraction : "+dOb.fraction());
    }
}

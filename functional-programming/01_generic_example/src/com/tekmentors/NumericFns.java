package com.tekmentors;

public class NumericFns<T extends Number> {
    T num;

    public NumericFns(T num) {
        this.num = num;
    }

    public double reciprocal(){
        return 1/num.doubleValue();
    }

    //return fractional component
    public double fraction(){
        return num.doubleValue() - num.intValue();
    }
}

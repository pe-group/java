package com.tekmentors;

public class MyContainment<T> implements Containment<T> {
    T[] arrayRef;

    public MyContainment(T[] arrayRef) {
        this.arrayRef = arrayRef;
    }

    @Override
    public boolean contains(T o) {
        for (T x : arrayRef){
            if (x.equals(o)) return true;
            return false;
        }
        return false;
    }
}

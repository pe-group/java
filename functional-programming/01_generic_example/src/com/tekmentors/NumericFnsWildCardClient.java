package com.tekmentors;

public class NumericFnsWildCardClient {

    public static void main(String[] args){
        NumericFnsWildCard<Double> dNumericFns = new NumericFnsWildCard<>(45.0);
        NumericFnsWildCard<Integer> fNumericFns = new NumericFnsWildCard<>(-45);

        System.out.println(dNumericFns.absEqual(fNumericFns));
    }
}

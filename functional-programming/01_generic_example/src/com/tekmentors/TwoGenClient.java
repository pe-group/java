package com.tekmentors;

public class TwoGenClient {
    public  static void main(String[] args){
        TwoGen<Integer, String> twoGen = new TwoGen<>(78,"This is an example of double parameter type");

        twoGen.showTypes();
        int v1 = twoGen.getOb1();
        String value = twoGen.getOb2();
        System.out.println("v1 : "+v1);
        System.out.println("value : "+value);
    }
}

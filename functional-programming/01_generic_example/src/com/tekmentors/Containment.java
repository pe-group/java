package com.tekmentors;

public interface Containment<T> {
    boolean contains(T o);
}

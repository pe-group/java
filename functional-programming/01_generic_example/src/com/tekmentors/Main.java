package com.tekmentors;

public class Main {

    public static void main(String[] args) {
	    // Create a Gen reference for Integers
        Gen<Integer> iOb = new Gen<>(88);

        // show the type of data used by iOb
        iOb.showType();

        int v = iOb.getOb();
        System.out.println("value : "+v);

        //Create a Gen object for Strings
        Gen<String> strOb = new Gen<>("This is a string object");

        //show the type of data used by strOb
        strOb.showType();
        String str = strOb.getOb();
        System.out.println("value : "+str);

    }
}

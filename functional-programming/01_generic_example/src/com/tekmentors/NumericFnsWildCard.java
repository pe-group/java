package com.tekmentors;

public class NumericFnsWildCard<T extends Number> {
    T num;

    public NumericFnsWildCard(T num) {
        this.num = num;
    }

    public T getNum() {
        return num;
    }

    public double reciprocal(){
        return 1/num.doubleValue();
    }

    //return fractional component
    public double fraction(){
        return num.doubleValue() - num.intValue();
    }

    public boolean absEqual(NumericFnsWildCard<?> number){
        if (Math.abs(num.doubleValue()) == Math.abs(number.getNum().doubleValue())){
            return true;
        }
        return false;
    }
}

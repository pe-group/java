package com.sapient.cabbooking;

public interface CabLocator {
    Cab findCab(String currentLocation);
}

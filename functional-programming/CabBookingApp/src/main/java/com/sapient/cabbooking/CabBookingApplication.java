package com.sapient.cabbooking;


public class CabBookingApplication {
    private CabLocator cabLocator = new OlaLocatorImpl();
    private CabNotifier cabNotifier;


    public String bookCab(String destination, String currentLocation) {
        Cab cab = cabLocator.findCab(currentLocation);
        if(cab !=null){
            boolean response = cabNotifier.notifyDriver(cab, destination);
            if (response)
                return "";
            return "Cab not available";
        }
        return "Cab not available";

    }
}

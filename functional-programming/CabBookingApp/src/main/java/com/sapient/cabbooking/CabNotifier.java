package com.sapient.cabbooking;

public interface CabNotifier {
    boolean notifyDriver(Cab cab, String destination);
}

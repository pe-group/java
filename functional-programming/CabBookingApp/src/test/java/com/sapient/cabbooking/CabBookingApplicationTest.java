package com.sapient.cabbooking;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CabBookingApplicationTest {


    @Test
    public void shouldProvideMeAValidCabInformationForAValidInputRequest(){
        String destination ="Sector 47";
        String currentLocation = "Sector 21";
        CabBookingApplication bookingApplication = new CabBookingApplication();
        String response =  bookingApplication.bookCab(destination, currentLocation);
        assertEquals("Cab with Number xyz and driver with name ABC",response);
    }
}

Please select an operation:

  0 - List inventory
  1 - Find item by quantity and price
  2 - List items by price
  3 - Count items by part name
  4 - Find items by installation time
  5 - End program: 0
  
brakes: [EBC 1hrs [EX8426:2 $125.35], Acme 3hrs [AC25:1 $90.74, AC26:4 $130.22]]
tires: [Goodyear 2hrs [RX2041:4 $75.25, SX3355:2 $90.5], Firestone 1hrs [FS2112:3 $60.47, FS2479:5 $85.2]]
Please select an operation:
  0 - List inventory
  1 - Find item by quantity and price
  2 - List items by price
  3 - Count items by part name
  4 - Find items by installation time
  5 - End program: 1
Select a part name: tires
Select a minimum quantity: 4
Select a price limit: 80.00
tires by Goodyear:
RX2041:4 $75.25
tires by Firestone:
Please select an operation:
  0 - List inventory
  1 - Find item by quantity and price
  2 - List items by price
  3 - Count items by part name
  4 - Find items by installation time
  5 - End program: 2
Select a part name: brakes
AC25:1 $90.74
EX8426:2 $125.35
AC26:4 $130.22
Please select an operation:
  0 - List inventory
  1 - Find item by quantity and price
  2 - List items by price
  3 - Count items by part name
  4 - Find items by installation time
  5 - End program: 3
Select a part name: tires
14
Please select an operation:
  0 - List inventory
  1 - Find item by quantity and price
  2 - List items by price
  3 - Count items by part name
  4 - Find items by installation time
  5 - End program: 4
Select a part name: tires
Select installation time limit: 2
Firestone 1hrs [FS2112:3 $60.47, FS2479:5 $85.2]
Please select an operation:
  0 - List inventory
  1 - Find item by quantity and price
  2 - List items by price
  3 - Count items by part name
  4 - Find items by installation time
  5 - End program: 5
Bye Bye.

1. Create a stream containing the names “Robert”, “Cheryl”, “Rachel”, “Jose”, and “Rita”. Remove all names that do not begin with the letter R. Sort the remaining elements by reverse ordering, and then print the results.

2. class Class
{
    String subject;
    Collection<Student> students;
    public Class(String su, Student... st)
    {
       subject = su;
       students = Arrays.asList(st);
    }
}

class Student
{
  String name;
  int id;
  float average
}

Using the example above, compute and display the following
a) The average grade point average of all the students
b) The average grade point average of the biology students
c) The average grade point average of the physics students



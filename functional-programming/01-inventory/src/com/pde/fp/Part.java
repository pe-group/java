package com.pde.fp;

import java.util.ArrayList;
import java.util.List;

public class Part {
	String name;
    String manu;
    int hoursToInstall;
    List<Item> models;
    public Part(String n, String m, int h, Item... it) 
    {
        name = n;
        manu = m;
        hoursToInstall = h;
        models = new ArrayList<>();
        for (Item i : it)
            models.add(i);
    }
    @Override
    public String toString()
    {
        return manu + " " + hoursToInstall + "hrs " + models;
    }
}

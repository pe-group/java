package com.pde.fp;

public class Item {
	String model;
    int quantity;
    double price;
    public Item(String m, int q, double p)
    {
        model = m;
        quantity = q;
        price = p;
    }

    @Override
    public String toString() 
    { 
        return model + ":" + quantity + " $" + price; 
    }
}

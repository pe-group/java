package com.tekmentors.chain.stream;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class StreamCreationExample1 {

  public static void main(String[] args){
    StreamCreationExample1 example1 = new StreamCreationExample1();
    example1.streamCreationMethod();
    System.out.println();
    example1.streamCreationMethodUsingIterator();
    System.out.println();
    example1.createStreamUsingConcatMethod();
    System.out.println();
    example1.createStreamUsingSupplierMethod();
    System.out.println();
    example1.createStreamUsingBuilderMethod();
    System.out.println();
    example1.createStreamUsingBuilderMethodTwo();
    System.out.println();
    example1.createStreamUsingBuilderMethodThree();
  }

  void streamCreationMethod(){
    Stream.empty().forEach(System.out::println);

    Stream.of(1).forEach(System.out::println);

    List<String> list = Arrays.asList("1","2");
    list.stream().forEach(System.out::print);
  }

  void streamCreationMethodUsingIterator(){
    Stream.iterate(1, i -> ++i).limit(9).forEach(System.out::print);
  }

  void createStreamUsingConcatMethod(){
    Stream<Integer> stream1 = Stream.of(1,2,3,4);
    Stream<Integer> stream2 = Stream.of(6,7,8,9);

    Stream.concat(stream1, stream2).forEach(System.out::print);
  }

  void createStreamUsingSupplierMethod(){
    Stream.generate(() -> 1).limit(5).forEach(System.out::print); //prints: 11111

    Stream.generate(() -> new Random().nextDouble()).limit(5).forEach(System.out::print); //prints: 5 random double number
  }

  void createStreamUsingBuilderMethod(){
    Stream.<String>builder().add("cat").add("dog").add("cow").build().forEach(System.out::print);
  }

  void createStreamUsingBuilderMethodTwo(){
    Stream.Builder<String> builder = Stream.builder();
    Stream.of("Cow ", "Dog ", "Cat").forEach(builder);
    builder.build().forEach(System.out::print);
  }

  void createStreamUsingBuilderMethodThree(){
    List<String> animals = Arrays.asList("Cat ", "Dog ", "Bear");
    buildStream(animals).forEach(System.out::print);
  }

  Stream<String> buildStream(List<String> values){
    Stream.Builder<String> builder = Stream.builder();
    for(String s: values){
      if(s.contains("a")){
        builder.accept(s);
      }
    }
    return builder.build();
  }
}

package com.tekmentors.chain.stream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamPersonFile {

  public static void main(String[] args) throws IOException {
    createPersonObjectOptimized();
  }

  public static void createPersonObject() throws IOException {
    List<Person> persons = new ArrayList<>();
    Path path = Paths.get("/Users/arun/training/sapient-productengg/java/functional-programming/04_chain_consumer", "person.csv");
    try(Stream<String> lines = Files.lines(path)){
        lines.forEach(s -> {
          String[] record = s.split(",");
          int age = Integer.valueOf(record[0].trim());
          String name = record[1].trim();
          persons.add(new Person(age, name));
        });
    }

    System.out.println(persons);
  }

  public static void createPersonObjectOptimized() throws IOException {
    List<Person> persons = new ArrayList<>();
    Path path = Paths.get("/Users/arun/training/sapient-productengg/java/functional-programming/04_chain_consumer", "person.csv");
   Stream<String> lines = Files.lines(path);
      persons = lines.map(s -> s.split(","))
                    .map(rec -> {
                          int age = Integer.valueOf(rec[0].trim());
                          String name = rec[1].trim();
                          return new Person(age, name);
                    })
                    .collect(Collectors.toList());


    persons.stream().forEach(p -> System.out.println(p));
  }

  public static void createPersonObjectOptimizedVersion2() throws IOException {
    List<Person> persons = new ArrayList<>();
    Path path = Paths.get("/Users/arun/training/sapient-productengg/java/functional-programming/04_chain_consumer", "person.csv");
    try(Stream<String> lines = Files.lines(path)){
      persons = lines.map(s -> s.split(","))
                     .map(rec -> {
                       return createPerson(rec);
                     }).collect(Collectors.toList());
    }

    persons.stream().forEach(System.out::println);
  }

  private static Person createPerson(String[] rec) {
    int age = Integer.valueOf(rec[0].trim());
    String name = rec[1].trim();
    return new Person(age, name);
  }
}

class Person {
  private int age;
  private String name;

  public Person(int age, String name) {
    this.age = age;
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return "Person{" +
      "age=" + age +
      ", name='" + name + '\'' +
      '}';
  }
}

package com.tekmentors.chain.stream;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.stream.Stream;

public class StreamExampleWithFile {

  public static void  main(String[] args) throws IOException {
    readStreamOfLinesUsingFiles();
    System.out.println("---------------------");
    readFileUsingNonStreamingWay();
    System.out.println("---------------------");
    readStreamOfLinesUsingFilesWithTryResourcesMechanism();
  }

  public static void readStreamOfLinesUsingFiles() throws IOException {
    Stream<String> lines = Files.lines(Paths.get("/Users/arun/training/sapient-productengg/java/functional-programming/04_chain_consumer", "data.txt"));
    lines.forEach( System.out::println);

    //close the stream and it's underlying file as well
    lines.close();
  }

  public static void readFileUsingNonStreamingWay() throws IOException {
    File file = new File("/Users/arun/training/sapient-productengg/java/functional-programming/04_chain_consumer/data.txt");
    FileReader fr = new FileReader(file);
    BufferedReader br = new BufferedReader(fr);

    String line;
    while((line = br.readLine())!=null){
      System.out.println(line);
    }
    br.close();
    fr.close();
  }

  public static void readStreamOfLinesUsingFilesWithTryResourcesMechanism() throws IOException {
    Path path = Paths.get("/Users/arun/training/sapient-productengg/java/functional-programming/04_chain_consumer", "data.txt");
    // The stream hence file will also be closed here
    try( Stream<String> lines = Files.lines(path)){
      lines.forEach( System.out::println);
    }
  }


}

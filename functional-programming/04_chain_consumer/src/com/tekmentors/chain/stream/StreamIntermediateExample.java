package com.tekmentors.chain.stream;



import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class StreamIntermediateExample {

  public static void main(String[] args){
    Stream.of("3", "2", "3", "4", "2").distinct()
      .forEach(System.out::print);  //prints: 324
    List<String> list = Arrays.asList("1", "2", "3", "4", "5");
    list.stream().skip(3).forEach(System.out::print);         //prints: 45
    list.stream().limit(3).forEach(System.out::print);        //prints: 123
    list.stream().filter(s -> Objects.equals(s, "2"))
      .forEach(System.out::print);  //prints: 2

    StreamIntermediateExample example = new StreamIntermediateExample();
    List<Integer> numbers = Arrays.asList(12,5,40,6,1,30);
    int result = example.findSumOfNumberInTheListWhichIsHigherThan10(numbers);
    System.out.println("RESULT - "+result);
  }

  public int findSumOfNumberInTheListWhichIsHigherThan10(List<Integer> numbers){
      return numbers.stream().filter(i -> i> 10).mapToInt(i -> i).sum();
  }
}

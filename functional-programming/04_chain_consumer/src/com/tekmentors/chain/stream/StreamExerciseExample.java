package com.tekmentors.chain.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamExerciseExample {


    public static void main(String[] args){
      StreamExerciseExample example = new StreamExerciseExample();
      List<Integer> numbers = Arrays.asList(12,5,40,6,1,30);
      int result = example.findSumOfNumbersFromTheGivenList(numbers);
      System.out.println("Sum of all numbers - > "+result);

      int result1 = example.findSumOfOnlyThoseNumbersWhichAreGreaterThan10(numbers);
      System.out.println("Sum of all numbers greater than 10 - > "+result1);

      int result2 = example.findSumOfSquaresOfOnlyThoseNumbersWhichAreGreaterThan10(numbers);
      System.out.println("Sum of squares of all numbers greater than 10 - > "+result2);

      example.createAnEmployeeMapFromList();
    }

    public int findSumOfNumbersFromTheGivenList(List<Integer> numbers){
      return numbers.stream().mapToInt(i -> i).sum();
    }

    public int findSumOfOnlyThoseNumbersWhichAreGreaterThan10(List<Integer> numbers){
      return numbers.stream().filter(i -> i>10).mapToInt(i -> i).sum();
    }

    public int findSumOfSquaresOfOnlyThoseNumbersWhichAreGreaterThan10(List<Integer> numbers){
      return numbers.stream().filter(i -> i > 10).mapToInt(i -> i * i).sum();
    }

    public Map<Integer, Employee> createAnEmployeeMapFromList(){
      List<Employee> employees = new ArrayList<>(
        Arrays.asList(
          new Employee(1,"A", 100),
          new Employee(2,"B", 200),
          new Employee(3,"C", 300),
          new Employee(4,"D", 400),
          new Employee(5,"E", 500),
          new Employee(6,"F", 600)
        )
      );

      Map<Integer, Employee> employeeMap = employees.stream().collect(Collectors.toMap(Employee::getId, Function.identity()));
      System.out.println(employeeMap);
      return employeeMap;
    }
}

class Employee {
  private int id;
  private String name;
  private double salary;

  public Employee(int id, String name, double salary) {
    this.id = id;
    this.name = name;
    this.salary = salary;
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public double getSalary() {
    return salary;
  }

  @Override
  public String toString() {
    return "Employee{" +
      "id=" + id +
      ", name='" + name + '\'' +
      ", salary=" + salary +
      '}';
  }
}

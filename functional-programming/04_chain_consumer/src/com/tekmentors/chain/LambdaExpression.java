package com.tekmentors.chain;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LambdaExpression {

    public static void main(String[] args){
        final  String xyz = "abc";

        RateCalculator rateCalculator = (rate,sum) -> {
            System.out.println(xyz);
            return rate * sum /100;
        };

        double v = rateCalculator.calculateRate(5.0, 100);
        System.out.println(v);

        String[] arr = new String[]{"a", "b", "c"};
        Stream<String> streamOfArrayFull = Arrays.stream(arr);
        Stream<String> streamOfArrayPart = Arrays.stream(arr, 1, 3);
        streamOfArrayPart.forEach(System.out::print);
//
//        Function<Double, Double> rateInterface = (sum) -> 5.0*sum;
//        rateInterface.apply(100.0);

        List<Integer> numbers = Arrays.asList(1,45,7,65,90);
        List<Integer> collect = numbers.stream().filter(n -> n > 45).collect(Collectors.toList());

        Integer reduce = numbers.stream().reduce(1, (a, b) -> a * b);
        System.out.println("Sum = "+reduce);

    }
}


interface  RateCalculator {
    double calculateRate(double rate, double sum);
}

 class RateCalculatorImpl implements RateCalculator {
    int value;
    @Override
    public double calculateRate(double rate, double sum) {
        System.out.println("rate--"+rate);
        this.value = 67;
        return 0;
    }
}
package com.tekmentors.chain;

import java.util.function.Function;

public class LambdaExpressionDemo {

  private Function<Integer, Double> createMultiplyBy(double num) {
    Function<Integer, Double> func = i -> i * num;
    return func;
  }
  private  Function<Double, Long> createSubtractInt(int num){
    Function<Double, Long> func = dbl -> Math.round(dbl - num);
    return func;
  }

}

package com.tekmentors.chain;

import java.util.function.Function;

public class IdentityDemo {

  public static void main(String[] args){
    IdentityDemo demo = new IdentityDemo();
    Function<Integer, Integer> id = Function.identity();
    System.out.println(id.apply(4));

    Function<Double, Double> multiplyByFive = Function.identity();
    System.out.println(multiplyByFive.apply(2.));

    Function<Double, Long> subtract7 = demo.createSubtractInt(7);
    System.out.println(subtract7.apply(11.0));

    long r = multiplyByFive.andThen(subtract7).apply(2.);
    System.out.println(r);
  }

  private Function<Integer, Double> createMultiplyBy(double num) {
    Function<Integer, Double> func = new Function<Integer, Double>() {
      public Double apply(Integer i) {
        return i * num;
      }
    };
    return func;
  }
  private  Function<Double, Long> createSubtractInt(int num){
    Function<Double, Long> func = new Function<Double, Long>(){
      public Long apply(Double dbl){
        return Math.round(dbl - num);
      }
    };
    return func;
  }
}

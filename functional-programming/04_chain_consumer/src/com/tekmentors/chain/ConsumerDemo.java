package com.tekmentors.chain;

import java.util.function.Consumer;

public class ConsumerDemo {

  public Consumer<Double> createPrintingFunc(String prefix, String suffix){
    Consumer<Double> func = new Consumer<Double>() {

      @Override
      public void accept(Double num) {
        System.out.println(prefix+ num + suffix);
      }
    };
    return func;
  }

  public static void main(String[] args){
    ConsumerDemo consumerDemo = new ConsumerDemo();
    Consumer<Double> print21By = consumerDemo.createPrintingFunc("21 by ", "");
    Consumer<Double> equalsBy21 = consumerDemo.createPrintingFunc("equals", " by 21");

    print21By.andThen(equalsBy21).accept(15d);
  }

}

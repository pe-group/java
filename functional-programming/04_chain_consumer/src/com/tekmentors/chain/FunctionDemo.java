package com.tekmentors.chain;

import java.util.function.Function;

public class FunctionDemo {
  public static void main(String[] args) {
    FunctionDemo demo = new FunctionDemo();
    Function<Integer, Double> multiplyBy5 = demo.createMultiplyBy(5d);
    System.out.println(multiplyBy5.apply(2));

    Function<Double, Long> subtractBy7 = demo.createSubtractInt(7);
    System.out.println(subtractBy7.apply(10d));

    long result = multiplyBy5.andThen(subtractBy7).apply(3);
    System.out.println(result);

  }

  private Function<Integer, Double> createMultiplyBy(double num) {
    Function<Integer, Double> func = new Function<Integer, Double>() {
      public Double apply(Integer i) {
        return i * num;
      }
    };
    return func;
  }
  private  Function<Double, Long> createSubtractInt(int num){
    Function<Double, Long> func = new Function<Double, Long>(){
      public Long apply(Double dbl){
        return Math.round(dbl - num);
      }
    };
    return func;
  }
}

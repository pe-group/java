package com.tekementors.nio.channel;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class BasicFileChannel {

  public void readDataFromAFileIntoBuffer() throws IOException {
    RandomAccessFile file = new RandomAccessFile("src/main/resources/data.txt", "rw");
    //Create a file channel from the file
    FileChannel channel = file.getChannel();

    ByteBuffer buffer = ByteBuffer.allocate(48);
    int bytesRead = channel.read(buffer); //read into buffer

    while (bytesRead !=-1){
      System.out.println("Read "+bytesRead);
      buffer.flip();  // make buffer ready for read

      while (buffer.hasRemaining()){
        System.out.print((char) buffer.get());    //read 1 byte at a time
      }

      buffer.clear();   // make buffer ready for writing

      bytesRead = channel.read(buffer);
    }
    file.close();
  }
}

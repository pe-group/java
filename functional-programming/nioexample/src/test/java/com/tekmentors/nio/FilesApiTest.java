package com.tekmentors.nio;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FilesApiTest {
  String HOME = System.getProperty("user.home");

  @Test
  public void checkIfFileExistsInSpecifiedPath() {
    Path path = Paths.get("/Users/arun/training/sapient-productengg/java/functional-programming/nioexample/src/main/resources", "data.txt");
    assertTrue(Files.exists(path));
  }

  @Test
  public void checkIfDirectoryExistsInSpecifiedPath() {
    Path path = Paths.get("/Users/arun/training/java87");
    assertFalse(Files.exists(path));
  }

  @Test
  public void checkIfTheSpecifiedFileIsARegularFileOrDirectory() {
    Path path = Paths.get("/Users/arun/training/sapient-productengg/java/functional-programming/nioexample/src/main/resources");
    assertFalse(Files.isRegularFile(path));

    assertTrue(Files.isDirectory(path));
  }

  @Test
  public void checkIfTheExistingDirectoryHasReadablePermission() {
    Path path = Paths.get("/Users/arun/training/sapient-productengg/java/functional-programming/nioexample/src/main/resources");
    assertTrue(Files.isReadable(path));

    assertTrue(Files.isExecutable(path));
  }

  @Test
  public void checkIfTwoFilesAreSame() throws IOException {
    Path path1 = Paths.get("/Users/arun/training/sapient-productengg/java/functional-programming/nioexample/src/main/resources", "data.txt");
    Path path2 = Paths.get("/Users/arun/training/sapient-productengg/java/functional-programming/nioexample/src/main/resources", "data.txt");
    assertTrue(Files.isSameFile(path1,path2));
  }

  @Test
  public void createNewDirectory_IftheDirectoryDoesNotExist() throws IOException {
    String dirName = "myDir_"+ UUID.randomUUID().toString();
    Path p = Paths.get(HOME+"/"+dirName);
    assertFalse(Files.exists(p));

    Files.createDirectory(p);

    assertTrue(Files.exists(p));
  }
}

import java.util.function.Predicate;

public class Main {

    public static void main(String[] args) {

        System.out.println("Hello World!");

        Main main = new Main();
        Predicate<Double> isSmallerThan20 = main.testSmallerThan(20d);
        System.out.println(isSmallerThan20.test(15d));
        Predicate<Double> isGreaterThan10 = main.testGreaterThan(10d);
        System.out.println(isGreaterThan10.test(15d));

        boolean b = isSmallerThan20.and(isGreaterThan10).test(15d);
        boolean b1 = isSmallerThan20.and(isGreaterThan10).test(8d);
        boolean b2 = isSmallerThan20.or(isGreaterThan10).test(8d);

        System.out.println("b = "+true+ " , b1 = "+b1+ " , b2= "+b2);

    }

    Predicate<Double> testSmallerThan(double limit){
        return new Predicate<Double>() {
            @Override
            public boolean test(Double num) {
                System.out.println("Test if "+num + " is smaller than "+limit);
                return num < limit;
            }
        };
    }

    Predicate<Double> testGreaterThan(double limit){
        Predicate<Double> func = new Predicate<Double>() {

            @Override
            public boolean test(Double num) {
                System.out.println("Test if "+num+" is greater than "+limit);
                return num > limit;
            }
        };

        return func;
    }
}


